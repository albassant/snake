﻿using UnityEngine;
using System.Collections;

public class Food : FieldObject
{
    public void RemoveFromField()
    {
        DestroyImmediate(this.gameObject);
    }

	public override void RemoveFromFieldAnimated(System.Action animFinished = null)
	{
		base.RemoveFromFieldAnimated(() => { RemoveFromField(); });
	}
}
