﻿using UnityEngine;
using System.Collections;

public class MainCamera : MonoBehaviour
{
    private static MainCamera instance;

    public static MainCamera Instance
    {
        get
        {
            if (instance == null)
                instance = GameObject.FindObjectOfType<MainCamera>();
            return instance;
        }
    }

    private Vector3 bottomLeftCameraPoint;
    private Vector3 worldCenter;
    private Vector2 worldDimensions;

    public Vector2 WorldDimensions { get { return worldDimensions; } }
    public Vector3 WorldCenter { get { return worldCenter; } }

    void Awake()
    {
        bottomLeftCameraPoint = Camera.main.ViewportToWorldPoint(Vector3.zero);
        var topRightCameraPoint = Camera.main.ViewportToWorldPoint(Vector3.one);
        worldCenter = Camera.main.ViewportToWorldPoint(Vector3.one * 0.5f);

        bottomLeftCameraPoint.z = topRightCameraPoint.z = worldCenter.z = 0f;

        worldDimensions = new Vector2(topRightCameraPoint.x - bottomLeftCameraPoint.x, topRightCameraPoint.y - bottomLeftCameraPoint.y);
    }
}
