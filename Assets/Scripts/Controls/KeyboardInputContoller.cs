﻿using UnityEngine;
using System.Collections;

public class KeyboardInputContoller : MonoBehaviour
{
    public System.Action<Snake.EDirection> OnUserChangedDirection;

	void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
            NotifySubscribers(Snake.EDirection.Left);

        else if (Input.GetKeyDown(KeyCode.RightArrow))
            NotifySubscribers(Snake.EDirection.Right);

        else if (Input.GetKeyDown(KeyCode.UpArrow))
            NotifySubscribers(Snake.EDirection.Up);

        else if (Input.GetKeyDown(KeyCode.DownArrow))
            NotifySubscribers(Snake.EDirection.Down);
    }

    void NotifySubscribers(Snake.EDirection newDir)
    {
        if (OnUserChangedDirection != null)
            OnUserChangedDirection(newDir);
    }
}
