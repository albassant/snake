﻿using UnityEngine;
using System.Collections;

public class FieldObject : MonoBehaviour
{
    protected IntVector position;

    public virtual IntVector Position
    {
        get { return position; }
        set
        {
            position = value;
            this.transform.position = Field.Instance.GameToWorld(position);
        }
    }

    private Vector3 size;
    public Vector3 Size
    {
        get { return size; }
        set
        {
            size = value;
            this.transform.localScale = size;
        }
    }

    void Awake()
    {
        Size = new Vector3(Field.Instance.CellWidth, Field.Instance.CellHeight, 1f);
    }

	public virtual void RemoveFromFieldAnimated(System.Action animFinished = null)
    {
		StartCoroutine(FadeOut(animFinished));
    }

	IEnumerator FadeOut(System.Action animFinished = null)
    {
        Vector3 scale = this.transform.localScale;

        for (float f = 1f; f >= 0f; f -= 0.1f)
        {
            scale *= f;
            this.transform.localScale = scale;
            yield return null;
        }

		if (animFinished != null)
			animFinished();
    }
}
