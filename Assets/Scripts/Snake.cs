﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Snake : MonoBehaviour
{
    public int Size = 2;
    public float movementFrequency = 0.6f;
    public SnakeCell SnakeCellPrefab;
    private List<SnakeCell> cells = new List<SnakeCell>();

    public System.Action<IntVector> OnPositionUpdated;

    public bool IsDead { get; private set; }

    public enum EDirection
    {
        Left,
        Right,
        Up,
        Down,
    }

    private EDirection direction;
    Coroutine updatePosCor;

    bool initialized = false;
    void Start()
    {
        int dir = Random.Range(0, 4);
        direction = (EDirection)dir;

        if (Size > 1)
            Size += 1; // make head a double-cell for cute animation effect 
        for (int i = 0; i < Size; i++)
            IncreaseSnake();

        initialized = true;
    }

    public void IncreaseSnake()
    {
        var pos = GetPositionForNewCell();
        SnakeCell snake = Instantiate<SnakeCell>(SnakeCellPrefab);
        snake.transform.parent = this.transform;
        snake.Position = pos;
        cells.Add(snake);
    }

    IntVector GetPositionForNewCell()
    {
        if (cells.Count == 0) // if the very first cell
        {
            int xPos = Random.Range(0, Field.Instance.Width);
            int yPos = Random.Range(0, Field.Instance.Height);
            return new IntVector(xPos, yPos);
        }
        
        var lastCell = cells[cells.Count - 1];
        
		if (lastCell == cells[0] || initialized) // make head a double-cell for cute animation effect 
        {
            return lastCell.Position;
        }

        //else if not initialized
        int x = direction == EDirection.Left ? lastCell.Position.X + 1 : (direction == EDirection.Right ? lastCell.Position.X - 1 : lastCell.Position.X);
        int y = direction == EDirection.Down ? lastCell.Position.Y + 1 : (direction == EDirection.Up ? lastCell.Position.Y - 1 : lastCell.Position.Y);

        return new IntVector(x, y);
    }

    float curTime = 0f;
    void Update()
    {
        if (IsDead)
            return;

        if (curTime >= movementFrequency)
        {
            UpdateState();
            curTime = 0f;
        }
        else
        {
            curTime += Time.deltaTime;
        }
    }

    public void UpdateDirection(EDirection newDir)
    {
        if (IsDead)
            return;

        if (newDir == EDirection.Left && direction == EDirection.Right
			|| newDir == EDirection.Right && direction == EDirection.Left
			|| newDir == EDirection.Up && direction == EDirection.Down
			|| newDir == EDirection.Down && direction == EDirection.Up)
			return;

        direction = newDir;
   }

    void UpdateState()
    {
        IntVector posOffset = new IntVector(0, 0);
        switch (direction)
        {
            case EDirection.Left: posOffset.X = -1; break;
            case EDirection.Right: posOffset.X = 1; break;
            case EDirection.Up: posOffset.Y = 1; break;
            case EDirection.Down: posOffset.Y = -1; break;
        }

        IntVector newPos = cells[0].Position + posOffset;
        if (IsPositionValid(newPos))
        { 
            if (OnPositionUpdated != null)
                OnPositionUpdated(newPos);

            float animTime = movementFrequency * 0.5f;
            if (cells.Count > 1)
            {
                // animate tail
                cells[cells.Count - 1].animTime = animTime;
                cells[cells.Count - 1].UpdatePosition(cells[cells.Count - 2].Position);
            }

            // animate head
            cells[0].animTime = animTime;
            cells[0].UpdatePosition(newPos, () =>
            {
			    // update other parts positions
                for (int i = cells.Count - 2; i > 0; --i)
                {
                    cells[i].UpdatePosition(cells[i - 1].Position, null, false);
                }
            });
        }
        else
        {
            IsDead = true;
            StartCoroutine(Die());
        }
    }

    bool IsPositionValid(IntVector snakePos)
    {
        return (!CollidedSnakeCell(snakePos) && !IsOutOfField(snakePos));
    }

	bool IsOutOfField(IntVector snakePos)
	{
		return (snakePos.X < 0 || snakePos.X >= Field.Instance.Width
			|| snakePos.Y < 0 || snakePos.Y >= Field.Instance.Height);
	}

    public bool CollidedSnakeCell(IntVector posToCollide)
    {
        for (int i = 1; i < cells.Count; i++)
        {
			if (posToCollide.Equals(cells[i].Position))
                return true;
        }

        return false;
    }

	IEnumerator Die()
    {
		for (int i = 0; i < cells.Count - 1; i++)
        {
			cells[i].RemoveFromFieldAnimated(); 
			yield return null;
        }

		cells[cells.Count - 1].RemoveFromFieldAnimated(() => 
		{
			DestroyImmediate(this.gameObject);	
		});
    }
}
