﻿using UnityEngine;
using System.Collections;

public class IntVector
{
    public int X;
    public int Y;

    public IntVector(int x, int y)
    {
        X = x;
        Y = y;
    }

    public int SqrMagnitude
    {
        get { return X * X + Y * Y; }
    }

    public Vector3 UnityVector
    {
        get { return new Vector3(X, Y, 0f); }
    }

    public static IntVector operator +(IntVector v1, IntVector v2)
    {
        return new IntVector(v1.X + v2.X, v1.Y + v2.Y);
    }

    public static IntVector operator -(IntVector v1, IntVector v2)
    {
        return new IntVector(v1.X - v2.X, v1.Y - v2.Y);
    }

    public bool Equals(IntVector other)
    {
        if (other == null)
            return false;

        return this.X == other.X && this.Y == other.Y;
    }

    public override string ToString()
    {
        return string.Format("({0}, {1})", this.X, this.Y);
    }
}
