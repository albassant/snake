﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    Food FoodPrefab;

	[SerializeField]
	Snake SnakePrefab;

    public int MaxFoodPieces = 3;

    private bool IsGameOver = false;

    List<Food> foodList = new List<Food>();
    private Snake snake;
    private KeyboardInputContoller inputController;

    public System.Action OnGameOver;

	void Start()
    {
        inputController = GameObject.FindObjectOfType<KeyboardInputContoller>();
        if (inputController == null)
        {
            Debug.LogError("inputController object not found!");
        }

		StartGame();
    }

    IEnumerator GenerateFood()
    {
        while (!IsGameOver)
        {
            if (foodList.Count < MaxFoodPieces)
            {
                Food food = Instantiate<Food>(FoodPrefab);
                food.Position = GetFreePos();
                foodList.Add(food);
            }

            yield return new WaitForSeconds(5f);
        }
    }

    IntVector GetFreePos()
    {
        int x = Random.Range(0, Field.Instance.Width);
        int y = Random.Range(0, Field.Instance.Height);

		IntVector pos = new IntVector(x, y);

		while (snake.CollidedSnakeCell(pos))
		{
			pos.X = Random.Range(0, Field.Instance.Width);
			pos.Y = Random.Range(0, Field.Instance.Height);
		}

        return pos;
    }

    void CheckCollisions(IntVector snakePos)
    {
        CheckFoodCollision(snakePos);
    }

    void CheckFoodCollision(IntVector snakePos)
    {
        if (IsGameOver)
            return;

        foreach (var food in foodList)
        {
            if (snakePos.Equals(food.Position))
            {
                snake.IncreaseSnake();
                foodList.Remove(food);
                food.RemoveFromFieldAnimated();
                break;
            }
        }
    }

    void Update()
    {
        if (snake.IsDead)
            FinishGame();
    }

    void UpdateSnakeDirection(Snake.EDirection newDir)
    {
        snake.UpdateDirection(newDir);
    }

    void FinishGame()
    {
        IsGameOver = true;

		inputController.OnUserChangedDirection -= UpdateSnakeDirection;
        StopAllCoroutines();

        snake.OnPositionUpdated -= CheckCollisions;

        foreach (var food in foodList)
        {
            food.RemoveFromFieldAnimated();
        }

        foodList.Clear();

        if (OnGameOver != null)
            OnGameOver();
    }

	public void StartGame()
	{
		IsGameOver = false;

		snake = Instantiate<Snake>(SnakePrefab);
		snake.transform.position = Vector3.zero;
		snake.transform.localScale = Vector3.one;
		snake.OnPositionUpdated += CheckCollisions;

		inputController.OnUserChangedDirection += UpdateSnakeDirection;

		StartCoroutine(GenerateFood());
	}
}
