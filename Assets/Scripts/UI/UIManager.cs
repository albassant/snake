﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    private GameManager gameManager;

    public Button startButton;
    public GameObject panel;

    void Start()
    {
        panel.SetActive(false);
        gameManager = GameObject.FindObjectOfType<GameManager>();
        gameManager.OnGameOver += Show;
	}
	
	void Show()
    {
        panel.SetActive(true);
    }

    public void StartGame()
    {
        panel.SetActive(false);
        gameManager.StartGame();
    }
}
