﻿using UnityEngine;
using System.Collections.Generic;

public class Field : MonoBehaviour
{
	public int Width = 10;
	public int Height = 10;
    private Vector3 fieldOffset;
    [HideInInspector]
    public float CellWidth;
    [HideInInspector]
    public float CellHeight;

    private static Field instance;
    public static Field Instance
    {
        get
        {
            if (instance == null)
                instance = GameObject.FindObjectOfType<Field>();
            return instance;
        }
    }

    void Start()
    {
        CellWidth = MainCamera.Instance.WorldDimensions.x / (float)Width;
        CellHeight = MainCamera.Instance.WorldDimensions.y / (float)Height;

        Vector3 worldCenterOffset = 0.5f * new Vector3(Width * CellWidth, Height * CellHeight);
        Vector3 cellCenterOffset = 0.5f * new Vector3(CellWidth, CellHeight);
        fieldOffset = MainCamera.Instance.WorldCenter - worldCenterOffset + cellCenterOffset;       
    }

    #region position conversion
    public IntVector WorldToGame(Vector3 worldPos)
    {
        int shiftedPosX = Mathf.RoundToInt((worldPos.x - fieldOffset.x) / CellWidth);
        int shiftedPosY = Mathf.RoundToInt((worldPos.y - fieldOffset.y) / CellHeight);
        return new IntVector(Mathf.Clamp(shiftedPosX, 0, Width - 1), Mathf.Clamp(shiftedPosY, 0, Height - 1));
    }

    public Vector3 GameToWorld(IntVector gamePos)
    {
        var shiftedPos = new Vector3(gamePos.X * CellWidth, gamePos.Y * CellHeight) + fieldOffset;
        return shiftedPos;
    }
    #endregion
}
 