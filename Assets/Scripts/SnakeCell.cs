﻿using UnityEngine;
using System.Collections;

public class SnakeCell : FieldObject
{
    public AnimationCurve curve;
 
    [HideInInspector]
    public float animTime = 0.5f;

    public void UpdatePosition(IntVector pos, System.Action animCallback = null, bool animated = true)
    {
        if (!animated)
        {
            Position = pos;
            return;
        }

        position = pos;
		this.StopAllCoroutines();
        StartCoroutine(UpdatePositionCor(animCallback));
    }

    IEnumerator UpdatePositionCor(System.Action animCallback)
    {
        Vector3 start = this.transform.position;
        Vector3 target = Field.Instance.GameToWorld(Position);
        Vector3 direction = target - start;

        float curTime = 0.0f;
        while (curTime < animTime)
        {
            curTime += Time.deltaTime;
            float t = curTime / animTime;

			Vector3 positionNow = start + direction * curve.Evaluate(t);
            this.transform.position = positionNow;

            yield return null;
        }

        if (animCallback != null)
            animCallback();
    }

	public override void RemoveFromFieldAnimated(System.Action animFinished = null)
    {
        StopAllCoroutines();
        base.RemoveFromFieldAnimated(animFinished);
    }
}
